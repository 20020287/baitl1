import java.io.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Locale;
import java.util.Scanner;
import Upgrade.Trie;
import com.sun.speech.freetts.VoiceManager;
import com.sun.speech.freetts.Voice;

public class DictionaryManagement {
    Dictionary myList = new Dictionary();
    Trie trieSearch = new Trie();
    public boolean change = false;

    public void insertFromCommandline() {
        Scanner sc = new Scanner(System.in);
        System.out.print("The number of words you want to insert: ");
        System.out.println();

        int num = 0;
        boolean check = false;
        String a =sc.next();
        while(true) {
            for(int i = 0; i < a.length(); i++) {
                if(a.charAt(i) < '0' || a.charAt(i) > '9') {
                    System.out.print("Invalid number, please enter a positive integer number");
                    break;
                }
                if(i == a.length()-1) check = true;
            }
            if(check) break;
            a = sc.next();
        }
        for(int i = 0; i < a.length(); i++) {
            num *= 10;
            num += a.charAt(i) - '0';
        }
        String tmp = sc.nextLine();
        int i = 0;
        while (i != num) {
            insertWord();
            i++;
        }
    }

    public void insertFromFile() throws FileNotFoundException {
        Scanner sc = new Scanner(new File("dictionaries.txt"));
        while (sc.hasNext()) {
            String readWord = sc.nextLine();
            Scanner token = new Scanner(readWord).useDelimiter("\t");
            Word newWord = new Word();
            newWord.setWord_target(token.next());
            newWord.setWord_explain(token.next());
            trieSearch.insert(newWord.getWord_target(), newWord.getWord_explain());
            myList.word_list.add(newWord);
            token.close();
        }
        myList.sortWords();
    }

    public void dictionaryLookup() {
        System.out.println("The word you want to look up: ");
        Scanner sc = new Scanner(System.in);
        String word = sc.nextLine().trim().toLowerCase();
        Word findWord = new Word(word);
        int search = Collections.binarySearch(myList.word_list, findWord);
        if (search >= 0) {
            Word check = myList.word_list.get(search);
            D_voice(check.getWord_target());
            System.out.println(word + ": " + check.getWord_explain());
        } else {
            System.out.println("This word doesn't exist!");
            System.out.println("Please give me the meaning of the word: ");
            Word newWord = new Word();
            newWord.setWord_target(word);
            newWord.setWord_explain(sc.nextLine().trim().toLowerCase());
            myList.word_list.add(newWord);
            trieSearch.insert(newWord.getWord_target(), newWord.getWord_explain());
            System.out.println("Successful!");
        }
    }

    public void insertWord() {
        System.out.println("The word you want to insert: ");
        Scanner sc = new Scanner(System.in);
        String word = sc.nextLine().trim().toLowerCase();
        boolean flag = false;
        int search = Collections.binarySearch(myList.word_list, new Word(word));
        if (search >= 0) {
            System.out.println("This word already exists!");
        } else {
            change = true;
            Word newWord = new Word();
            System.out.println("The meaning of the word: ");
            newWord.setWord_target(word);
            newWord.setWord_explain(sc.nextLine().trim().toLowerCase());
            myList.word_list.add(newWord);
            trieSearch.insert(newWord.getWord_target(), newWord.getWord_explain());
            myList.sortAfterChange();
            System.out.println("Successful!");
        }
    }

    public void removeWord() {
        Scanner sc = new Scanner(System.in);
        System.out.println("The word you want to remove: ");
        String word = sc.nextLine().trim().toLowerCase();
        Word existedWord = new Word(word);
        int search = Collections.binarySearch(myList.word_list, existedWord);
        if (search >= 0) {
            myList.word_list.remove(search);
            trieSearch.delete(existedWord.getWord_target());
            change = true;
            System.out.println("Successful!");
        } else {
            System.out.println("This word doesn't exist!");
        }
    }

    public void editWord() {
        Scanner sc = new Scanner(System.in);
        System.out.println("The word you want to edit: ");
        String word = sc.nextLine().trim().toLowerCase();
        Word existedWord = new Word(word);
        int search = Collections.binarySearch(myList.word_list, existedWord);
        if (search >= 0) {
            myList.word_list.remove(search);
            trieSearch.delete(existedWord.getWord_target());
            Word newWord = new Word();
            System.out.println("Edit to: ");
            newWord.setWord_target(sc.nextLine().trim().toLowerCase());
            System.out.println("The meaning of the word: ");
            newWord.setWord_explain(sc.nextLine().trim().toLowerCase());
            myList.word_list.add(newWord);
            trieSearch.insert(newWord.getWord_target(), newWord.getWord_explain());
            myList.sortAfterChange();
            System.out.println("Successful!");
            change = true;
        } else {
            System.out.println("This word doesn't exist!");
        }
    }

    public void dictionaryExportToFile() throws IOException {
        FileWriter fileWriter = new FileWriter("dictionaries.txt");
        PrintWriter printWriter = new PrintWriter(fileWriter);
        for (Word check : myList.word_list) {
            printWriter.println(check.getWord_target() + "\t" + check.getWord_explain());
        }
        printWriter.close();
    }
    public void dictionarySearch() {
        System.out.println("Enter some characters: ");
        String s;
        Scanner scanner = new Scanner(System.in);
        s = scanner.nextLine();
        System.out.println("All words starting with: " + s);
        myList.word_list.forEach((i) -> {
            int index = i.getWord_target().indexOf(s);
            if (index == 0) {
                System.out.println(i.getWord_target() + "\t|" + i.getWord_explain());
            }
        });
    }

    public void advancedSearch() {
        System.out.println("Enter some characters: ");
        String prefix;
        Scanner scanner = new Scanner(System.in);
        prefix = scanner.nextLine().trim().toLowerCase();
        System.out.println("All words starting with: " + prefix);
        ArrayList<String> list = (ArrayList<String>) trieSearch.keysWithPrefix(prefix);
        Collections.sort(list);
        for (String s: list) {
            System.out.println(s);
        }
    }

    public static void D_voice(String a) {
        System.setProperty("freetts.voices", "com.sun.speech.freetts.en.us.cmu_us_kal.KevinVoiceDirectory");
        Voice voice = VoiceManager.getInstance().getVoice("kevin");
        voice.allocate();
        try {
            voice.setRate(125);
            voice.speak(a);
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void translateOnline() throws IOException {
        Scanner sc = new Scanner(System.in);
        System.out.println("Choose the language you want to translate from:");
        System.out.println("1. E or e for English");
        System.out.println("2. V or v for Vietnamese");
        String choice = sc.nextLine();
        choice = choice.toUpperCase();
        String readText;
        System.out.println("Enter text:");
        readText = sc.nextLine();

        switch (choice) {
            case "E":
                System.out.println(Translator.translate("en", "vi", readText));
                break;
            case "V":
                System.out.println(Translator.translate("vi", "en", readText));
                break;
            default:
                System.out.println("Wrong choice, you must to chose again! \n");
        }
    }


}

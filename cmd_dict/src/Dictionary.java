import java.util.ArrayList;
import java.util.Collections;

public class Dictionary {
    public ArrayList<Word> word_list = new ArrayList<>();

    public void sortWords() {
        Collections.sort(word_list);
    }

    public void sortAfterChange() {
        Word value = word_list.get(word_list.size() - 1);
        int j = word_list.size() - 2;
        while(j >= 0 && word_list.get(j).compareTo(value) > 0){
            word_list.set(j + 1,word_list.get(j));
            j = j - 1;
        }
        word_list.set(j + 1,value);
    }
}

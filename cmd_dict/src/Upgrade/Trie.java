package Upgrade;

import java.util.ArrayList;
import java.util.HashMap;

public class Trie {
    /**Nút gốc không lưu thông tin.*/
    private Node root;
    private int n;

    private static class Node {
        private String word_explain;
        private HashMap<Character,Node> next = new HashMap<>();
    }

    public Trie() {}

    /**Returns the meaning of the given word(key).*/
    public String get(String key) {
        if (key == null) throw new IllegalArgumentException("argument to get() is null");
        Node x = get(root, key, 0);
        if (x == null) return null;
        return x.word_explain;
    }

    private Node get(Node x, String key, int d) {
        if (x == null) return null;
        if (d == key.length()) return x;
        char c = key.charAt(d);
        return get(x.next.get(c), key, d+1);
    }

    /**Does this symbol table contain the given word(key)?*/
    public boolean contains(String key) {
        if (key == null) throw new IllegalArgumentException("argument to contains() is null");
        return get(key) != null;
    }

    /**Inserts the key-value pair into the symbol table.*/
    public void insert(String key, String meaning) {
        if (key == null) throw new IllegalArgumentException("first argument to insert() is null");
        root = put(root, key, meaning, 0);
    }

    private Node put(Node x, String key, String meaning, int d) {
        if (x == null) x = new Node();
        if (d == key.length()) {
            n++;
            x.word_explain = meaning;
            return x;
        }
        // 0    1  2  3  4  5<key.length>
        //root  a  p  p  l  e
        char c = key.charAt(d);
        //add the key-value pair
        x.next.put(c,put(x.next.get(c), key, meaning, d+1));
        return x;
    }

    public int size() {
        return n;
    }

    public boolean isEmpty() {
        return size() == 0;
    }

    public Iterable<String> keys() {
        return keysWithPrefix("");
    }

    /**Returns all keys in the set starting with prefix.*/
    public Iterable<String> keysWithPrefix(String prefix) {
        ArrayList<String> results = new ArrayList<String>();
        Node x = get(root, prefix, 0);
        collect(x, new StringBuilder(prefix), results);
        return results;
    }
    //                                          root
    //                                          /  \
    //                                         t   a
    //                                         |   |
    //                                         r   n
    //                                         |   |  \
    //                                         a   g   y(*)
    //                                         |   |
    //                                         n   l
    //                                         |   |
    //                                         s   e(*)
    //                                      /  |
    //                                     s   f
    //				                       |   |  \
    //				                       p   e   o
    //				                       |   |   |
    //				                       o   r   r
    //				                       |  (*)  |
    //				                       r       m(*)
    //				                       |       |
    //				                       t(*)    a
    //					                           |
    //					                           t
    //					                           |
    //					                           i
    //					                           |
    //					                           o
    //					                           |
    //				 	                           n(*)
    //
    //prefix: trans
    private void collect(Node x, StringBuilder prefix, ArrayList<String> results) {
        if (x == null) return;
        if (x.word_explain != null) results.add(prefix.toString());
        // next: i f
        for (Character c : x.next.keySet()) {
            prefix.append(c);
            collect(x.next.get(c), prefix, results);
            //trans
            prefix.deleteCharAt(prefix.length() - 1);
        }
    }

    /**Removes the key from the set if the key is present.*/
    public void delete(String key) {
        if (key == null) throw new IllegalArgumentException("argument to delete() is null");
        root = delete(root, key, 0);
    }

    private Node delete(Node x, String key, int d) {
        if (x == null) return null;
        if (d == key.length()) {
            if (x.word_explain != null) n--;
            x.word_explain = null;
        }
        else {
            char c = key.charAt(d);
            x.next.put(c,delete(x.next.get(c), key,d+1));
        }

        // remove sub trie rooted at x if it is completely empty
        if (x.word_explain != null) return x;
        for (char c: x.next.keySet())
            if (x.next.get(c) != null)
                return x;
        return null;
    }

}
